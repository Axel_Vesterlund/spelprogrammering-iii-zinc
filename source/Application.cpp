//Application.cpp

#include <zinc/ColorShader.h>
#include "Application.h"

struct Vertex
{
	float x, y, z;
	uint32 color;
};

Application::Application()
{
	m_window = nullptr;
	m_width = 0;
	m_height = 0;
}

Application::~Application()
{

}
bool Application::create(void* window, int32 width, int32 height)
{
	m_window = window;
	m_width = width;
	m_height = height;

	m_render_context = zinc::RenderContext::create(window, width, height);
	//m_triangle = zinc::RenderTechniqueTriangle::create("assets/first_shader.txt");
	m_shader = zinc::ColorShader::create("assets/first_shader.txt");

	Vertex vertices[]=
	{
		{ -1.0f,  1.0f, 0.0, 0xff0000ff },  //R�d
		{  1.0f,  1.0f, 0.0, 0xff00ff00 }, //Gr�n
		{  1.0f, -1.0f, 0.0, 0xffff0000 }, //Bl�

		{  1.0f, -1.0f, 0.0, 0xffff0000 }, //Bl�
		{ -1.0f, -1.0f, 0.0, 0xffffffff }, //Vit
		{ -1.0f,  1.0f, 0.0, 0xff0000ff }, //R�d
	};
	m_mesh = zinc::Mesh::create(sizeof(Vertex), sizeof(vertices) / sizeof(vertices[0]), vertices);

	return true;
}
void Application::destroy()
{
	m_render_context->destroy();
}
void Application::update()
{
	m_render_context->clear();

	m_shader->use();
	m_mesh->draw(0,6);
	//m_triangle->draw();

	m_render_context->present();
}