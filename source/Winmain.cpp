//Winmain.cpp

#define Unused(x) ((void)x)

#include <Windows.h>

#include <Application.h>

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CLOSE:
	{
		PostQuitMessage(0);
	} break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	Unused(hInstance);
	Unused(hPrevInstance);
	Unused(lpCmdLine);
	Unused(nCmdShow);
	

	WNDCLASSEX window_class = { 0 };
	window_class.cbSize = sizeof(WNDCLASSEX);
	window_class.hInstance = hInstance;
	window_class.lpszClassName = L"zincwindowclass";
	window_class.lpfnWndProc = WndProc;
	window_class.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	window_class.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	int result = RegisterClassEx(&window_class);
	if (result == 0)
		return 1;

	DWORD style = (WS_OVERLAPPEDWINDOW & ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX));

	int width = 1280;
	int height = 720;

	RECT rc = { 0, 0, width, height };
	AdjustWindowRect(&rc, style, FALSE);
	
	RECT deskrect = { 0 };
	HWND desktop = GetDesktopWindow();
	GetWindowRect(desktop, &deskrect);
	int centerx = deskrect.right / 2 - ((rc.right - rc.left) / 2);
	int centery = deskrect.bottom / 2 - ((rc.bottom - rc.top) / 2);
	

	HWND window = CreateWindowEx(0, window_class.lpszClassName, L"zinc",
		style, //Style
		centerx, centery, 
		rc.right - rc.left, rc.bottom - rc.top, //W, H
		NULL, NULL, window_class.hInstance, NULL);
	if (window == NULL)
		return 1;

	ShowWindow(window, nCmdShow);

	Application app;
	if (!app.create(window, width, height))
		return 3;
	
	bool running = true;
	MSG msg;
	while (running)
	{
		while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				running = false;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		app.update();
		Sleep(5);
	}

	app.destroy();

	return 0;
}