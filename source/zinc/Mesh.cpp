#include <cassert>

#include <d3d11.h>

#include <zinc/ServiceLocator.h>
#include <zinc/Render_Context.h>
#include <zinc/Mesh.h>

#define SAFE_RELEASE(x) if(x!=nullptr) {x->Release(); x = nullptr;}


namespace zinc
{
	Mesh::Ptr Mesh::create(uint32 size, uint32 count, const void* buffer)
	{
		return Ptr(new Mesh(size, count, buffer));
	}

	Mesh::Mesh(uint32 size, uint32 count, const void* buffer)
	{
		m_stride = size;

		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();
		m_context = render_context->get_context();

		D3D11_BUFFER_DESC buf_desc = { 0 };
		buf_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		buf_desc.ByteWidth = size * count;
		buf_desc.CPUAccessFlags = 0;
		buf_desc.MiscFlags = 0;
		buf_desc.StructureByteStride = 0;
		buf_desc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA data = { 0 };
		data.pSysMem = buffer;
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		HRESULT hr = device->CreateBuffer(&buf_desc, &data, &m_vertex_buffer);
		if (hr != S_OK)
		{
			assert(false && "could not create vertex buffer");
		}

	}
	Mesh::~Mesh()
	{
		SAFE_RELEASE(m_vertex_buffer);
	}

	void Mesh::draw(uint32 start, uint32 count)
	{
		const uint32 offset = 0;
		m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		m_context->IASetVertexBuffers(0, 1, &m_vertex_buffer, &m_stride, &offset);

		m_context->Draw(count, start);
	}

}