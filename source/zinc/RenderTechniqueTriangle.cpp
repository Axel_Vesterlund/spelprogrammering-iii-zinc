#include <stdio.h>
#include <cassert>

#include <d3d11.h>
#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")

#include <zinc/RenderTechniqueTriangle.h>
#include "zinc\ServiceLocator.h"
#include <zinc\Render_Context.h>
#define SAFE_RELEASE(x) if(x!=nullptr) {x->Release(); x = nullptr;}

namespace zinc
{
	RenderTechniqueTriangle::Ptr RenderTechniqueTriangle::create(const char* filename)
	{
		FILE* fin = fopen(filename, "r");
		assert(fin && "could not open shader source file");
		fseek(fin, 0, SEEK_END);
		uint32 size = (uint32)ftell(fin);
		fseek(fin, 0, SEEK_SET);
		
		char source[4096] = {0};
		assert(size < sizeof(source) && "shader source too big for source buffer");
		uint32 len = (uint32)fread(source, sizeof(char), size, fin);
		fclose(fin);

		source[len] = '\0';

		char* vertex_source = strstr(source, "[vertex]");
		assert(vertex_source);
		*vertex_source = '\0';
		vertex_source += 8;

		char* pixel_source = strstr(vertex_source, "[pixel]");
		assert(pixel_source);
		*pixel_source = '\0';
		pixel_source += 7;


		return Ptr(new RenderTechniqueTriangle(vertex_source, pixel_source));
	}
	RenderTechniqueTriangle::RenderTechniqueTriangle(const char* vertex_shader_source, const char* pixel_shader_source)
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();

		ID3D11VertexShader* vertex_shader = nullptr;
		ID3D11PixelShader* pixel_shader = nullptr;
		ID3DBlob* source = nullptr, *error = nullptr;
		ID3DBlob* layout = nullptr;


		HRESULT hr = D3DCompile(vertex_shader_source, strlen(vertex_shader_source), NULL, NULL, NULL, "main", "vs_5_0", 0, 0, &source, &error);
		if (hr != S_OK)
		{
			const char* error_message = (const char*)error->GetBufferPointer();
			OutputDebugStringA(error_message);
			assert(false && "error in vertex shader source");
		}

		hr = device->CreateVertexShader(source->GetBufferPointer(), source->GetBufferSize(), NULL, &vertex_shader);
		if (hr != S_OK)
		{
			assert(false && "could not create vertex shader");
		}

		D3DGetInputAndOutputSignatureBlob(source->GetBufferPointer(), source->GetBufferSize(), &layout);

		SAFE_RELEASE(source);
		SAFE_RELEASE(error);

		hr = D3DCompile(pixel_shader_source, strlen(pixel_shader_source), NULL, NULL, NULL, "main", "ps_5_0", 0, 0, &source, &error);
		if (hr != S_OK)
		{
			const char* error_message = (const char*)error->GetBufferPointer();
			OutputDebugStringA(error_message);
			assert(false && "could not compile pixel shader");
		}

		hr = device->CreatePixelShader(source->GetBufferPointer(), source->GetBufferSize(), NULL, &pixel_shader);
		if (hr != S_OK)
		{
			assert(false && "could not create pixel shader");
		}
		SAFE_RELEASE(source);
		SAFE_RELEASE(error);

		D3D11_INPUT_ELEMENT_DESC desc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, sizeof(float32)*3, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		ID3D11InputLayout* input_layout = nullptr;
		hr = device->CreateInputLayout(desc, sizeof(desc)/sizeof(desc[0]), layout->GetBufferPointer(), layout->GetBufferSize(), &input_layout);
		if (hr != S_OK)
		{
			assert(false && "could not create input layout");
		}

		
		struct
		{
			float32 x, y, z;
			uint32 color; //ABGR
		}
		vertices[] =
		{
			{ -1.0f, 1.0f, 0.0, 0xff0000ff },  //R�d
			{ 1.0f, 1.0f, 0.0, 0xff00ff00 }, //Gr�n
			{ 1.0f, -1.0f, 0.0, 0xffff0000 }, //Bl�

			{ 1.0f, -1.0f, 0.0, 0xffff0000 }, //Bl�
			{ -1.0f, -1.0f, 0.0, 0xffffffff }, //Vit
			{ -1.0f, 1.0f, 0.0, 0xff0000ff }, //R�d

		};

		D3D11_BUFFER_DESC buf_desc = { 0 };
		buf_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		buf_desc.ByteWidth = sizeof(vertices);
		buf_desc.CPUAccessFlags = 0;
		buf_desc.MiscFlags = 0;
		buf_desc.StructureByteStride = 0;
		buf_desc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA data = { 0 };
		data.pSysMem = vertices;
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		ID3D11Buffer* vertex_buffer = nullptr;
		hr = device->CreateBuffer(&buf_desc, &data, &vertex_buffer);
		if (hr!= S_OK)
		{
			assert(false && "could not create vertex buffer");
		}

		m_vertex_shader = vertex_shader;
		m_pixel_shader = pixel_shader;
		m_vertex_buffer = vertex_buffer;
		m_input_layout = input_layout;
		m_context = render_context->get_context();

	}
	RenderTechniqueTriangle::~RenderTechniqueTriangle()
	{

	}
	void RenderTechniqueTriangle::draw()
	{
		uint32 stride = sizeof(float32) * 3 + sizeof(uint32);
		uint32 offset = 0;

		m_context->VSSetShader(m_vertex_shader, NULL, 0);
		m_context->PSSetShader(m_pixel_shader, NULL, 0);
		m_context->IASetInputLayout(m_input_layout);
		m_context->IASetVertexBuffers(0, 1, &m_vertex_buffer, &stride, &offset);
		m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		m_context->Draw(6, 0);
	}
}