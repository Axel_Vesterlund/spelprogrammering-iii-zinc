#include <stdio.h>
#include <cassert>
#include <d3d11.h>

#include "zinc\ServiceLocator.h"
#include <zinc\Render_Context.h>
#include <zinc/ColorShader.h>

namespace zinc
{
	ColorShader::Ptr ColorShader::create(const char* filename)
	{
		FILE* fin = fopen(filename, "r");
		assert(fin && "could not open shader source file");
		fseek(fin, 0, SEEK_END);
		uint32 size = (uint32)ftell(fin);
		fseek(fin, 0, SEEK_SET);

		char source[4096] = { 0 };
		assert(size < sizeof(source) && "shader source too big for source buffer");
		uint32 len = (uint32)fread(source, sizeof(char), size, fin);
		fclose(fin);

		source[len] = '\0';

		char* vertex_source = strstr(source, "[vertex]");
		assert(vertex_source);
		*vertex_source = '\0';
		vertex_source += 8;

		char* pixel_source = strstr(vertex_source, "[pixel]");
		assert(pixel_source);
		*pixel_source = '\0';
		pixel_source += 7;

		return Ptr(new ColorShader(vertex_source, pixel_source));
	}

	ColorShader::ColorShader(const char* vertex_source, const char* pixel_source) : Shader(vertex_source, pixel_source)
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();

		D3D11_INPUT_ELEMENT_DESC desc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, sizeof(float32) * 3, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		HRESULT hr = device->CreateInputLayout(desc, sizeof(desc) / sizeof(desc[0]), m_layout_signature->GetBufferPointer(), m_layout_signature->GetBufferSize(), &m_input_layout);
		if (hr != S_OK)
		{
			assert(false && "could not create input layout");
		}
	}

	ColorShader::~ColorShader()
	{

	}
}