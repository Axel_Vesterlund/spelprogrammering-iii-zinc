#ifndef RENDERTECHNIQUETRIANGLE_H_INCLUDED
#define RENDERTECHNIQUETRIANGLE_H_INCLUDED

#include <Zinc.h>

struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11Buffer;
struct ID3D11InputLayout;
struct ID3D11DeviceContext;

namespace zinc
{
	class RenderTechniqueTriangle
	{
		RenderTechniqueTriangle(const RenderTechniqueTriangle&);
		RenderTechniqueTriangle& operator=(const RenderTechniqueTriangle&);

	public:
		typedef std::unique_ptr<RenderTechniqueTriangle> Ptr;

		static Ptr create(const char* filename);

	private:
		RenderTechniqueTriangle(const char* vertex_shader_source, const char* pixel_shader_source);

	public:
		~RenderTechniqueTriangle();

		void draw();

	private:
		ID3D11VertexShader* m_vertex_shader;
		ID3D11PixelShader* m_pixel_shader;
		ID3D11Buffer* m_vertex_buffer;
		ID3D11InputLayout* m_input_layout;
		ID3D11DeviceContext* m_context;
	};

}
#endif