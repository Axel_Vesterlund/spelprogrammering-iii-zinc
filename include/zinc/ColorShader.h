
#ifndef COLORSHADER_H_INCLUDED
#define COLORSHADER_H_INCLUDED

#include <Zinc.h>
#include <zinc/Shader.h>

namespace zinc
{
	class ColorShader : public Shader
	{
		ColorShader(const ColorShader&);
		ColorShader& operator=(const ColorShader&);

	public:
		typedef std::unique_ptr<ColorShader> Ptr;
		
		static Ptr create(const char* filename);

	private:
		ColorShader(const char* vertex_source, const char* pixel_source);

	public:
		~ColorShader();
	
	};
}
#endif // !COLORSHADER_H_INCLUDED
