
#ifndef SHADER_H_INCLUDED
#define SHADER_H_INCLUDED

#include <Zinc.h>

struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11InputLayout;
struct ID3D11DeviceContext;
struct ID3D10Blob;

namespace zinc
{
	class Shader{
		Shader(const Shader&);
		Shader& operator=(const Shader&);

	public:
		typedef std::unique_ptr<Shader> Ptr;

		static Ptr create(const char* filename);

	protected:
		Shader(const char* vertex_source, const char* pixel_source);

	public:
		virtual ~Shader();

		virtual void use();

	protected:
		ID3D11VertexShader* m_vertex_shader;
		ID3D11PixelShader* m_pixel_shader;
		ID3D11InputLayout* m_input_layout;
		ID3D11DeviceContext* m_context;
		ID3D10Blob* m_layout_signature;

	};
}

#endif