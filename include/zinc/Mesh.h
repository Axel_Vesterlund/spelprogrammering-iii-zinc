
#ifndef Mesh_H_INCLUDED
#define Mesh_H_INCLUDED

#include <Zinc.h>

struct ID3D11Buffer;
struct ID3D11DeviceContext;

namespace zinc
{
	class Mesh
	{
		Mesh(const Mesh&);
		Mesh& operator=(const Mesh&);

	public:
		typedef std::unique_ptr<Mesh> Ptr;

		static Ptr create(uint32 element_size, uint32 count, const void* buffer);
		
	private:
		Mesh(uint32 size, uint32 count, const void* buffer);

	public:
		~Mesh();

		void draw(uint32 start, uint32 count);

	private:
		uint32 m_stride;
		uint32 m_count;
		ID3D11DeviceContext* m_context;
		ID3D11Buffer* m_vertex_buffer;

	};
}

#endif