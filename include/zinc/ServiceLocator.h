#ifndef SERVICELOCATOR_H_INCLUDED
#define SERVICELOCATOR_H_INCLUDED

namespace zinc
{
	template <class Service>
	class ServiceLocator
	{

		ServiceLocator(const ServiceLocator<Service>&);
		ServiceLocator& operator=(const ServiceLocator<Service>&);

	public:
		ServiceLocator(){}

		static void set_service(Service* service)
		{
			ms_service = service;
		}

		static Service* get_service()
		{
			return ms_service;
		}

	private:
		static Service* ms_service;
	};

	template <class Service>
	Service* ServiceLocator<Service>::ms_service = nullptr;
}

#endif