#ifndef RENDERCONTEXT_H_INCLUDED
#define RENDERCONTEXT_H_INCLUDED

#include <zinc.h>

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;

namespace zinc
{
	class RenderContext
	{
		RenderContext(const RenderContext&);
		RenderContext& operator=(const RenderContext&);

	public:
		typedef std::unique_ptr<RenderContext> Ptr;

		static Ptr create(void* window, int32 width, int32 height);

	public:
		~RenderContext();

		void destroy();

		void clear();
		void present();

		ID3D11Device* get_device();
		ID3D11DeviceContext* get_context();

	private:
		RenderContext();

	private:
		IDXGISwapChain* m_swap_chain;
		ID3D11Device* m_device;
		ID3D11DeviceContext* m_context;
		ID3D11RenderTargetView* m_rtv;
	};
}
#endif