//Application.hpp

#ifndef APPLICATION_H_INCLUDED
#define APPLICATION_H_INCLUDED

#include <Zinc.h>
#include <zinc/render_context.h>
//#include <zinc/RenderTechniqueTriangle.h>
#include <zinc/shader.h>
#include <zinc/mesh.h>

class Application
{
	Application(const Application&);
	Application& operator=(const Application&);
	
public:
	Application();
	~Application();

	bool create(void* window, int32 width, int32 height);
	void destroy();
	void update();

private:
	void* m_window;
	int32 m_width;
	int32 m_height;


	zinc::RenderContext::Ptr m_render_context;
	//zinc::RenderTechniqueTriangle::Ptr m_triangle;
	zinc::Shader::Ptr m_shader;
	zinc::Mesh::Ptr m_mesh;
};

#endif //#define APPLICATION_HPP INCLUDED